/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc core module
 */

(function () {
  'use strict';
  angular.module('com.module.core', []);

})();
