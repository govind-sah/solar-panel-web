/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this hold configuration required for landing page
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .config(config);

     config.$inject = ['$stateProvider','$urlRouterProvider','$httpProvider'];
    function config($stateProvider,$urlRouterProvider,$httpProvider) {

      // default route
      $urlRouterProvider.otherwise('/');

      // Now set up the states
      $stateProvider
        .state('page', {
          url: '',
          abstract: true,
          templateUrl: 'scripts/modules/core/views/page.html'
        })
        .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'scripts/modules/core/views/app.html',
          controller: 'AppCtrl',
          controllerAs: 'main'
        });

      $httpProvider.interceptors.push('RequestInterceptor');
    }
})();
