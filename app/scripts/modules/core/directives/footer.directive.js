/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc custom footer directive
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .directive("customFooter", customFooter);

  function customFooter() {
    var directive = {
      restrict: 'E',
      templateUrl: 'scripts/modules/core/views/footer.html',
      replace: true,
      scope: {
        admin: "="
      }
    };

    return directive;

  }
})();
