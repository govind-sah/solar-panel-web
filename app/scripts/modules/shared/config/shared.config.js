/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this hold configuration required for shared module
 */

(function () {
  'use strict';

  angular
    .module('com.module.shared')
    .config(config);

  config.$inject = [];
  function config() {

  }

})();
