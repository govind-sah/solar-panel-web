/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc main content header directive
 */

(function () {
  'use strict';

  angular
    .module('com.module.shared')
    .directive("contentHeader", contentHeader);

  function contentHeader() {
    var directive = {
      restrict: 'E',
      templateUrl: 'scripts/modules/shared/partials/content-header.html',
      replace: true,
      scope: {
        currentPageState: "="
      }
    };

    return directive;

  }
})();
