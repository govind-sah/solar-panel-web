/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc custom header directive
 */

(function () {
  'use strict';

  angular
    .module('com.module.core')
    .directive("pwCheck", pwCheck);

  function pwCheck() {
    var directive = {
      require: 'ngModel',
      link: link
    };

    return directive;

    function link(scope, elem, attrs, ctrl) {
      var firstPassword = '#' + attrs.pwCheck;
      elem.add(firstPassword).on('keyup', function () {
        scope.$apply(function () {
          var v = elem.val() === $(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });
    }

  }

})();
