/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold function to edit profile
 */

(function () {
  'use strict';

  angular
    .module('com.module.users')
    .controller('EditUserProfileCtrl', EditUserProfileCtrl);

  EditUserProfileCtrl.$inject = ['AdminService','ApiService', '$scope', '$state', '$stateParams', 'ngDialog','toastr'];
  function EditUserProfileCtrl(AdminService,ApiService, $scope, $state, $stateParams, ngDialog, toastr) {
    var vm = this;

    vm.user = null
    vm.selectAllFlag = false;
    var openedDialog =null;

    vm.message = '';
    vm.currentPageState = $state.current.stateDesc;

    vm.confirm = confirm;
    vm.updateConfirm = updateConfirm;
    vm.isExists = isExists;
    vm.selectAll = selectAll;
    vm.toggle = toggle;

    initController();

    function initController() {
      loadSelectedUser();
      getAllPlants();
    }

    function loadSelectedUser() {
      var payload = {
        projection: {"_id": 1,"fullName": 1, "company": 1, "designation": 1, "email": 1, "phone": 1 , "permissions" : 1 , "plants" : 1 },
        criteria: {},
        options: {}
      };

      AdminService.GetById($stateParams.id, payload)
        .then(function (response) {
          vm.data = response.data;
          console.log(vm.data.plants[0]);
          if(vm.data.plants[0] == 'all'){
           vm.selectAllFlag = true;
          }
        });
    }

    function confirm(){
      vm.message = {preText:'You are editing other user profile!',midText:' Be Careful',postText:' ?'};

      openedDialog=ngDialog.openConfirm({
        template: 'scripts/modules/shared/partials/confirm-popup.html',
        className: 'ngdialog-theme-default',
        scope: $scope
      });
    }


   vm.example1model = [];
    vm.plants = [];


   function getAllPlants(){

     var payload = {
       criteria: {},
       projection: {
         plantName: 1
       },
       options: {}
     };

     ApiService.GetAll('/powerPlants', payload)
       .then(function (response) {
        vm.plants =response.data;
       });
   }

    vm.isProcessing = false;
    function updateConfirm() {
      vm.isProcessing = true;
      if(vm.selectAllFlag){
        vm.data.plants = ["all"];
      }
      AdminService.Update(vm.data)
        .then(function (response) {
          vm.isProcessing = false;

          if (response.success) {
            toastr.success('Updated successfully!');
          }
          ngDialog.close(openedDialog);
        });
    }

    function isExists(id){
     return vm.data.plants.indexOf(id) > -1;
    }

    function toggle(id){
        var index = vm.data.plants.indexOf(id);
       if(index > -1){
         vm.data.plants.splice(index,1);  //already exists
       }
      else{
         vm.data.plants.push(id);  //new addition
       }
    }

    function selectAll(){
      vm.data.plants = [];
      if(vm.selectAllFlag) {
        (vm.plants).forEach(function(data){
          vm.data.plants.push(data._id);
        });
      }
    }



  }

})();
