/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold function for reports related
 */

(function() {
    'use strict';

    angular
        .module('com.module.users')
        .controller('ReportsCtrl', ReportsCtrl);

    ReportsCtrl.$inject = ['ApiService', '$state', 'ENV', '$rootScope'];

    function ReportsCtrl(ApiService, $state, ENV, $rootScope) {
        var vm = this;
        vm.isProcessing = false;
        vm.isProcessingReport = false;


        //Scope variables goes here
        vm.currentPageState = $state.current.stateDesc;

        //function declartions here
        vm.getLogReport = getLogReport;
        vm.getDailyReport = getDailyReport;
        vm.date1Change = date1Change;
        vm.date2Change = date2Change;
        vm.incDecReportDate = incDecReportDate;
        vm.incDecLogDate = incDecLogDate;
        var baseUrl = ENV.apiUrl;


        // date-picker goes here
        vm.date1 = new Date();
        vm.date2 = new Date();

        vm.open1 = open1;
        vm.open2 = open2;

        vm.dateOptions1 = {
            formatYear: 'yyyy',
            maxDate: new Date(),
            minMode: 'month',
            startingDay: 1
        };

        vm.dateOptions2 = {
            formatYear: 'yy',
            maxDate: new Date(),
            startingDay: 1
        };

        function open1() {
            vm.popup1.opened = true;
        }

        function open2() {
            vm.popup2.opened = true;
        }

        vm.format1 = 'MMMM yyyy';
        vm.format2 = 'dd MMMM yyyy';

        vm.popup1 = {
            opened: false
        };

        vm.popup2 = {
            opened: false
        };

        var userID = $rootScope.globals.currentAdmin._id;

        (function initController() {
            // getLogReport();

        }());

        function getLogReport() {
            vm.isProcessing = true;
            if (new Date(vm.date1).setHours(0, 0, 0, 0) !== new Date().setHours(0, 0, 0, 0)) { //comparing if today
                var date = new Date(vm.date1);
                vm.date1 = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            }
            ApiService.GetCSV('/reports/logBook?date=' + vm.date1 + '&_id=' + userID).then(function(res) {
                if (res) {
                    vm.isProcessing = false;
                    ApiService.FileDownload(res, "log-book");
                }
            });
        }

        function getDailyReport() {
            vm.isProcessingReport = true;
            ApiService.GetCSV('/reports/dailyReport?date=' + vm.date2 + '&_id=' + userID).then(function(res) {
                if (res) {
                    vm.isProcessingReport = false;
                    ApiService.FileDownload(res, "daily-report");
                }
            });
        }

        // download report when month changed
        function date1Change() {
            getLogReport();
        }

        // Update energy graph when date changed
        function date2Change() {
            getDailyReport();
        }

        function incDecReportDate(opr) {
            var localDate = new Date(vm.date2);
            if (opr == '-') {
                vm.date2 = new Date(localDate.setDate(localDate.getDate() - 1));
            } else {
                vm.date2 = new Date(localDate.setDate(localDate.getDate() + 1));
            }
        }

        function incDecLogDate(opr) {
            var localDate = new Date(vm.date1);
            if (opr == '-') {
                vm.date1 = new Date(localDate.setMonth(localDate.getMonth() - 1));
            } else {
                vm.date1 = new Date(localDate.setMonth(localDate.getMonth() + 1));
            }
        }

    }

})();
