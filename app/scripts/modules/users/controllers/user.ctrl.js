/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold functions related to users
 */

(function() {
    'use strict';

    angular
        .module('com.module.users')
        .controller('UserCtrl', UserCtrl);

    UserCtrl.$inject = ['$state', '$scope', '$rootScope', 'ngDialog', '$compile', 'ENV', 'DTOptionsBuilder', 'DTColumnBuilder', 'AuthenticationService', 'AdminService', 'toastr', 'ApiService'];

    function UserCtrl($state, $scope, $rootScope, ngDialog, $compile, ENV, DTOptionsBuilder, DTColumnBuilder, AuthenticationService, AdminService, toastr, ApiService) {
        var vm = this;

        vm.currentPageState = $state.current.stateDesc;
        var openedDialog = null;
        var isDeleteOpr; // delete-> true else -> update

        vm.message = '';
        vm.delete = deleteRow;
        vm.updateConfirm = updateConfirm;
        vm.updateUserAccess = updateUserAccess;

        vm.dtInstance = {};

        vm.dtOptions = DTOptionsBuilder.fromSource(ENV.apiUrl + '/datatable/admins')
            .withFnServerData(serverData)
            .withDataProp('data')
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withPaginationType('full_numbers')
            .withOption('createdRow', createdRow)
            .withDisplayLength(100)
            .withBootstrap()
            .withDOM('ftrlpi');


      function serverData(sSource, aoData, fnCallback, oSettings) {
            sSource = ENV.apiUrl + '/datatable/admins';
            var extraCond = { name: "criteria", value: { conditions: { userType: 1 } } };
            aoData.push(extraCond);

            oSettings.jqXHR = $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'headers': {
                    Authorization: 'Bearer ' + $rootScope.globals.currentAdmin.token
                },
                'data': { "data": JSON.stringify(aoData) },
                'success': fnCallback,
                'error': function(err) {
                    if (err.status == 401) {
                        toastr.clear();
                        toastr.warning('Your session has been expired!, Redirecting to login.');
                        AuthenticationService.ClearCredentials();
                        $state.go('page.login');
                    }
                }
            });
        }

        vm.dtColumns = [
            DTColumnBuilder.newColumn('fullName').withTitle('Full name'),
            DTColumnBuilder.newColumn('company').withTitle('Company'),
            DTColumnBuilder.newColumn('designation').withTitle('Designation'),
            DTColumnBuilder.newColumn('email').withTitle('Email ID'),
            DTColumnBuilder.newColumn('phone').withTitle('Phone Number'),
            DTColumnBuilder.newColumn('access').notVisible(),
            DTColumnBuilder.newColumn('_id').notVisible(),
            DTColumnBuilder.newColumn('emailVerification').notVisible()
        ];

        if ($rootScope.globals.currentAdmin.userType == 2) {

            vm.dtColumns.push(DTColumnBuilder.newColumn(null).withTitle('Status').withClass('request-btn')
                .renderWith(requestHtml));

            vm.dtColumns.push(DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('actions')
                .renderWith(actionsHtml));
        }



        function deleteRow(userId) {
            vm.message = { preText: 'Are you sure want to', midText: ' delete', postText: ' the selected user?' };
            isDeleteOpr = true;
            vm.updateData = {
                _id: userId
            };
            openDialog();
        }

        function updateUserAccess(userId, status) {

            if (status == 1) {
                vm.message = { preText: 'Are you sure want to', midText: ' approve', postText: ' the selected user?' };
            } else {
                vm.message = { preText: 'Are you sure want to', midText: ' decline', postText: ' the selected user?' };
            }

            isDeleteOpr = false;

            vm.updateData = {
                _id: userId,
                access: status
            };
            openDialog();
        }

        function openDialog() {
            openedDialog = ngDialog.openConfirm({
                template: 'scripts/modules/shared/partials/confirm-popup.html',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        }

        function updateConfirm() {
            vm.isProcessing = true;
            if (isDeleteOpr) {
                AdminService.Delete(vm.updateData._id)
                    .then(function(response) {
                        reloadDataTable(response);
                    });
            } else {
                ApiService.Update({ access: vm.updateData.access }, '/admins/access/' + vm.updateData._id)
                    .then(function(response) {
                        reloadDataTable(response);
                    });
            }
        }

        function reloadDataTable(response) {
            vm.isProcessing = false;
            if (response.success) {
                toastr.success('Deleted successfully!');
                vm.dtInstance.reloadData();
            }
            ngDialog.close(openedDialog);
        }

        function createdRow(row) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        }

        function actionsHtml(data) {
            return '<a class="btn btn-warning" ui-sref=app.editProfile({id:"' + data._id + '"})>' +
                '   <i class="fa fa-edit"></i>' +
                '</a>&nbsp;' +
                '<button class="btn btn-danger" ng-click=vm.delete("' + data._id + '")>' +
                '   <i class="fa fa-trash-o"></i>' +
                '</button>';
        }

        function requestHtml(data) {

            if(data.emailVerification == false){
                return 'Email Not Verified';
            }
            else if (data.access) {
                return 'Approved';
            } else if (data.access == -1) {
                return 'Declined';
            } else {
                return '<button class="btn btn-danger" ng-click=vm.updateUserAccess("' + data._id + '",-1)>' +
                    'Decline' +
                    '</button>' +
                    '<button class="btn btn-success" ng-click=vm.updateUserAccess("' + data._id + '",1)>' +
                    'Approve' +
                    '</button>';
            }
        }
    }
})();
