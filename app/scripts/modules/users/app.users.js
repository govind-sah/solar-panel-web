/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc user module
 */

(function () {
  'use strict';
  angular.module('com.module.users',[]);

})();
