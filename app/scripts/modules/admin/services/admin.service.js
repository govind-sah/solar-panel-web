/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc admin related services
 */


(function() {
    'use strict';

    angular
        .module('com.module.admin')
        .factory('AdminService', AdminService);

    AdminService.$inject = ['$http', 'ENV'];

    function AdminService($http, ENV) {
        var baseUrl = ENV.apiUrl + '/admins';

        var service = {
            GetAll: GetAll,
            GetById: GetById,
            Create: Create,
            Update: Update,
            Delete: Delete,
            SendInsToResetPassword: SendInsToResetPassword,
            ResetNewPassword: ResetNewPassword,
            emailVerification: emailVerification,
            ChangePassword: ChangePassword
        };

        return service;

        function GetAll() {
            return $http.get(baseUrl).then(handleSuccess, handleError);
        }

        function GetById(id, payload) {
            return $http.get(baseUrl + '/' + id, { params: payload }).then(handleSuccess, handleError);
        }

        function Create(admin) {
            return $http.post(baseUrl, admin).then(handleSuccess, handleError);
        }

        function Update(admin) {
            return $http.put(baseUrl + '/' + admin._id, admin).then(handleSuccess, handleError);
        }

        function Delete(id) {
            return $http.delete(baseUrl + '/' + id).then(handleSuccess, handleError);
        }

        function SendInsToResetPassword(email) {
            return $http.get(baseUrl + '/getResetPasswordToken/' + email).then(handleSuccess, handleError);
        }

        function ResetNewPassword(user) {
            return $http.put(baseUrl + '/resetPassword', user).then(handleSuccess, handleError);
        }

        function emailVerification(user) {
            return $http.post(baseUrl + '/emailVerify', user).then(handleSuccess, handleError);
        }


        function ChangePassword(user) {
            return $http.put(baseUrl + '/changePassword/' + user._id, user).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError() {
            return { success: false }
        }

    }

})();
