/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc admin module of the application.
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {

    // Now set up the states
    $stateProvider
      .state('page.login', {
        url: '/',
        stateDesc: {bodyClass: 'home-page', title: 'Login', description: 'Login here'},
        templateUrl: 'scripts/modules/admin/views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'vm'
      })
      .state('page.register', {
        url: '/register',
        stateDesc: {bodyClass: 'home-page', title: 'Register', description: 'Register here'},
        templateUrl: 'scripts/modules/admin/views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'vm'
      })
      .state('page.forgotPassword', {
        url: '/forgot_password',
        stateDesc: {
          bodyClass: 'home-page',
          title: 'Forgot Password',
          description: 'Send instruction to reset password here'
        },
        templateUrl: 'scripts/modules/admin/views/forgot-password.html',
        controller: 'ForgotPasswordCtrl',
        controllerAs: 'vm'
      })
      .state('page.resetPassword', {
        url: '/reset_password',
        stateDesc: {bodyClass: 'home-page', title: 'Reset Password', description: 'Reset password here'},
        templateUrl: 'scripts/modules/admin/views/reset-password.html',
        controller: 'ResetPasswordCtrl',
        controllerAs: 'vm'
      })

        .state('page.emailVerification', {
        url: '/emailVerification',
        stateDesc: {bodyClass: 'home-page', title: 'Email Verification', description: 'email verification here'},
        templateUrl: 'scripts/modules/admin/views/email-verification.html',
        controller: 'EmailVerificationCtrl',
        controllerAs: 'vm'
      })


      .state('app.dashboard', {
        url: '/dashboard',
        stateDesc: {bodyClass: 'dashboard-page', title: 'Power Plants Information', description: 'Listing of all power plants.'},
        templateUrl: 'scripts/modules/admin/views/dashboard.html',
        resolve: {
          resolveLogin: resolveLogin
        },
        controller: 'DashboardCtrl',
        controllerAs: 'vm'
      })
      .state('app.edit_profile', {
        url: '/edit_profile',
        stateDesc: {bodyClass:'profile', title: 'Your Profile', description: 'Edit your profile here'},
        templateUrl: 'scripts/modules/admin/views/edit-profile.html',
        controller: 'EditProfileCtrl',
        controllerAs: 'vm'
      });

    resolveLogin.$inject = ['$q', '$state', '$rootScope', '$timeout'];
    function resolveLogin($q, $state, $rootScope, $timeout) {
      var deferred = $q.defer();

      $timeout(function () {
        if ($rootScope.globals.currentAdmin) {
          deferred.resolve(null);
        }
        else {
          // $state.go('page.login');
          deferred.reject(null);
        }
      });
      return deferred.promise;
    }
  }

})();
