/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc admin module
 */

(function () {
  'use strict';
  angular.module('com.module.admin', []);

})();
