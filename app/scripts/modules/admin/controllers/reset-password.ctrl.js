/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold reset password function
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('ResetPasswordCtrl', ResetPasswordCtrl);

  ResetPasswordCtrl.$inject = ['AdminService', '$location', 'toastr'];
  function ResetPasswordCtrl(AdminService, $location, toastr) {
    var vm = this;
    vm.user = {};
    vm.resetPassword = resetPassword;
    vm.user.passwordResetToken = $location.search().token;
    vm.user.email = $location.search().email;

    function resetPassword() {
      vm.dataLoading = true;

      AdminService.ResetNewPassword(vm.user)
        .then(function (response) {
          if (response.success) {
            toastr.success('Password reset successfully!');
            vm.dataLoading = false;
            $location.search('token',null);
            $location.search('email',null);
            $location.path('/');
          }
          vm.dataLoading = false;
        });
    }

  }

})();
