/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold functions for admin profile
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('DashboardCtrl', DashboardCtrl);

  DashboardCtrl.$inject = ['$compile', 'toastr', '$scope', '$state', 'AuthenticationService', '$rootScope', 'ENV', 'DTOptionsBuilder', 'DTColumnBuilder', 'ApiService', 'ngDialog'];

  function DashboardCtrl($compile, toastr, $scope, $state, AuthenticationService, $rootScope, ENV, DTOptionsBuilder, DTColumnBuilder, ApiService, ngDialog) {
    var vm = this;

    vm.currentPageState = $state.current.stateDesc;
    vm.date = new Date();

    var openedDialog = null;
    var isDeleteOpr; // delete-> true else -> update

    vm.message = '';

    // Function declarations goes here
    vm.open1 = open1;
    vm.delete = deleteRow;
    vm.updateConfirm = updateConfirm;
    vm.refresh = refresh;

    vm.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(),
      startingDay: 1
    };

    var user = $rootScope.globals.currentAdmin;
    vm.permissions = user.permissions;

    function open1() {
      vm.popup1.opened = true;
    }

    vm.format = 'dd MMMM yyyy';

    vm.popup1 = {
      opened: false
    };

    vm.dtInstance = {};

    vm.dtOptions = DTOptionsBuilder.fromSource(ENV.apiUrl + '/admin/test/datatable/powerPlants')
      .withFnServerData(serverData)
      .withOption('createdRow', createdRow)
      .withDataProp('data')
      .withOption('processing', true)
      .withOption('serverSide', true)
      .withOption('responsive', {
        details: {
          renderer: renderer
        }
      })
      .withPaginationType('full_numbers')
      .withBootstrap()
      .withDisplayLength(100)
      .withDOM('trlpi')
      // .withFixedHeader({
      //     top: true
      //   })
      // .withOption('drawCallback', function(settings) {
      //   $timeout(function(){ angular.element($window).triggerHandler('resize') }, 200);
      // });

    vm.dtColumns = [
      DTColumnBuilder.newColumn('plantName').withTitle('Name'),
      DTColumnBuilder.newColumn('state').withTitle('State'),
      DTColumnBuilder.newColumn('city').withTitle('City'),
      DTColumnBuilder.newColumn('plantCapacity').withTitle('Capacity (kW)'),
      DTColumnBuilder.newColumn('energyToday').withTitle('Energy Today'),
      DTColumnBuilder.newColumn('kWh/kWp').withTitle('kWh/kWp'),
      DTColumnBuilder.newColumn('pr').withTitle('PR').withClass('minW80'),
      DTColumnBuilder.newColumn('energyLifetime').withTitle('Energy Lifetime'),
      DTColumnBuilder.newColumn(null).withTitle('Comm Status')
        .renderWith(commStatusHtml),
      DTColumnBuilder.newColumn(null).withTitle('Actions').withClass('actions')
        .renderWith(actionsHtml),
      DTColumnBuilder.newColumn(null).withTitle('More Details').withClass('none')
        .renderWith(detailsHtml),
      DTColumnBuilder.newColumn('_id').notVisible(),
      DTColumnBuilder.newColumn('gridDeviceMAC').notVisible(),
      DTColumnBuilder.newColumn('inverterEnergyCalculation').notVisible(),
      DTColumnBuilder.newColumn('commStatus').notVisible(),
      DTColumnBuilder.newColumn('plantStatus').notVisible()
    ];

    $('#search').keyup(function(){
      vm.dtInstance.DataTable.search($(this).val()).draw();
    })


    function deleteRow(path) {
      vm.message = {preText: 'Are you sure want to', midText: ' delete', postText: ' ?'};
      isDeleteOpr = true;
      vm.updateData = {
        path: path
      };
      openDialog();
    }

    function openDialog() {
      openedDialog = ngDialog.openConfirm({
        template: 'scripts/modules/shared/partials/confirm-popup.html',
        className: 'ngdialog-theme-default',
        scope: $scope
      });
    }

    function updateConfirm() {
      vm.isProcessing = true;
      if (isDeleteOpr) {
        ApiService.Delete(vm.updateData.path)
          .then(function (response) {
            reloadDataTable(response);
          });
      }
    }

    function reloadDataTable(response) {
      vm.isProcessing = false;
      if (response.success) {
        toastr.success('Deleted successfully!');
        vm.dtInstance.reloadData();
      }
      ngDialog.close(openedDialog);
    }

    function createdRow(row) {
      $compile(angular.element(row).contents())($scope);
    }

    function serverData(sSource, aoData, fnCallback, oSettings) {
      sSource = ENV.apiUrl + '/admin/test/datatable/powerPlants';

      oSettings.jqXHR = $.ajax({
        'dataType': 'json',
        'type': 'POST',
        'url': sSource,
        'headers': {
          Authorization: 'Bearer ' + $rootScope.globals.currentAdmin.token
        },
        'data': {"data": JSON.stringify(aoData), date: vm.date},
        'success': fnCallback,
        'error': function (err) {
          if (err.status == 401) {
            toastr.clear();
            toastr.warning('Your session has been expired!, Redirecting to login.');
            AuthenticationService.ClearCredentials();
            $state.go('page.login');
          }
        }
      });
    }

    function detailsHtml(data) {

      if (vm.permissions.editPlants) {
        var str = '<a class="btn btn-info pull-right" ui-sref=app.addDevice({id:"' + data._id + '"})> <i  class="fa fa-plus" aria-hidden="true"></i> Add Device</a> <br> <br>';
      } else {
        var str = '<br> <br>';
      }


      str = str + '<div class="row"> <div class="col-xs-4"> <div class="panel panel-info"> ' +
        '<div class="panel-heading">Energy Meter Now </div> <div class="panel-body inner-info">';


      var mi = 1; //meter index

      data.slideDown.energyMeterNow.forEach(function (item) {


        if (vm.permissions.editPlants) {
          str = str + '<div class="row"> <div class="col-xs-12 text-center text-info">' + item.name + ' (' + item.macAddress + ') <a  ui-sref=app.editDevice({id:"' + data._id + '",deviceId:"' + item._id + '"})>EDIT</a>' +
            ' <a href="javascript:void(0)"  class="text-danger" ng-click=vm.delete("/powerPlants/' + data._id + '/deviceMacs/' + item._id + '")>&nbsp;REMOVE</a></div> </div>';

        } else {
          str = str + '<div class="row"> <div class="col-xs-12 text-center text-info">' + item.name + ' (' + item.macAddress + ') ' +
            '</div> </div>';

        }


        item.meters.forEach(function (meter) {
          str = str + '<div class="row"> <div class="col-xs-6"><span>Meter' + mi + '</span></div> ' +
            '<div class="col-xs-6"><span>' + meter.value + ' kWh</span></div></div>';

          mi++;
        });

      });

      str = str + '</div></div></div>'; // closed col-xs-4 before info panel

      //import/export grid
      if(data.gridDeviceMAC && data.gridDeviceMAC !== ""){
        str = str + '<div class="col-xs-4"> <div class="panel panel-info"> ' +
          '<div class="panel-heading">Grid Import Export </div> <div class="panel-body inner-info">';
        data.slideDown.gridImportExport.forEach(function (item,i) {
          str += '<div class="row font-12">';
          str += '<div class="col-xs-12"><b>'+item.name+'</b></div>';
          str += '<div class="col-xs-4 text-info font-12">Export</div>';
          str += '<div class="col-xs-4 text-info font-12">Import</div>';
          str += '<div class="col-xs-4 text-info font-12">Total Grid Power  </div>';
          str +='</div>'


          item.meters.forEach(function (meter,i) {
            str += '<div class="row font-12"> <div class="col-xs-4"><span>' + meter.GridExport + ' kWh</div> ' +
              '<div class="col-xs-4"><span>' + meter.GridImport + ' kWh</span></div>' +
              '<div class="col-xs-4"><span>' + meter.TotalGridPower + ' kWh</span></div></div>';
          });

        });
        str = str + '</div></div></div>'; // closed col-xs-4 before info panel
      }



      //end gridimportexport

      str = str + '<div class="col-xs-4"> <div class="panel panel-info"> ' +
        '<div class="panel-heading">Statistics Today </div> <div class="panel-body inner-info">';

      data.slideDown.stat.forEach(function (stat) {
        str = str + '<div class="row"> <div class="col-xs-6"><span>' + stat.key + '</span></div> ' +
          '<div class="col-xs-6"><span>' + stat.value + '</span></div></div>';
      });


      str = str + '</div></div></div>'; // closed col-xs-4 before info panel

      str = str + '<div class="col-xs-4"> <div class="panel panel-info"> ' +
        '<div class="panel-heading"> <div class="row"> <div class="col-xs-4"> </div>' +
        '<div class="col-xs-4">Energy Today</div> <div class="col-xs-4">Power Now</div></div> </div> <div class="panel-body inner-info">';

      data.slideDown.invertersData.forEach(function (item) {

        if (item.name != '') {

          if (vm.permissions.editPlants) {
            str = str + '<div class="row"> <div class="col-xs-12 text-center text-info">' + item.name + ' (' + item.macAddress + ') <a  ui-sref=app.editDevice({id:"' + data._id + '",deviceId:"' + item._id + '"})>EDIT</a>' +
              '<a  href="javascript:void(0)" class="text-danger" ng-click=vm.delete("/powerPlants/' + data._id + '/deviceMacs/' + item._id + '")>&nbsp;REMOVE</a></div> </div>';


          } else {
            str = str + '<div class="row"> <div class="col-xs-12 text-center text-info">' + item.name + ' (' + item.macAddress + ') ' +
              '</div> </div>';

          }


        }

        item.inverters.forEach(function (iA) {
          str = str + '<div class="row"> <div class="col-xs-4"><span>' + iA.field + '</span></div> ' +
            '<div class="col-xs-4"><span>' + iA.energyToday + '</span></div> ' +
            '<div class="col-xs-4"><span>' + iA.powerNow + '</span></div></div>';
        });

      });

      str = str + '</div></div></div>';

      return str;
    }

    function actionsHtml(data) {
      return '<a class="btn btn-info" style="margin: auto;" target="_blank" ui-sref=app.showPlant({id:"' + data._id + '"})>' +
        '   <i class="fa fa-eye"></i>' +
        '</a>';
    }

    function commStatusHtml(data) {
      var myStyle = "color: red";

      if (data.commStatus == 1) {
        myStyle = "color: yellow";
      } else if (data.commStatus == 2) {
        myStyle = "color: green";
      }

      return '<i class="fa fa-circle" style="' + myStyle + '" aria-hidden="true"></i>';
    }

    function plantStatusHtml(data) {
      var myStyle = "color: red";

      if (data.plantStatus == 1) {
        myStyle = "color: yellow";
      } else if (data.plantStatus == 2) {
        myStyle = "color: green";
      }

      return '<i class="fa fa-circle" style="' + myStyle + '" aria-hidden="true"></i>';
    }

    function renderer(api, rowIdx, columns) {
      var data = $.map(columns, function (col, i) {
        return col.hidden ?
        '<li data-dtr-index="' + col.columnIndex + '" data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
        '<span class="dtr-title">' +
        col.title +
        '</span> ' +
        '<span class="dtr-data">' +
        col.data +
        '</span>' +
        '</li>' :
          '';
      }).join('');

      return data ?
        $compile(angular.element($('<ul data-dtr-index="' + rowIdx + '"/>').append(data)))($scope) :
        false;
    }

    // Update data-tables when date changed
    $scope.$watch(function () {
      return vm.date;
    }, function (value) {
      try {
        vm.dtInstance.reloadData();
      } catch (err) {
        console.log(err);
      }
    });

    function refresh() {
      vm.dtInstance.reloadData();
    }


  }

})();
