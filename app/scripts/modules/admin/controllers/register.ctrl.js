/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold register function
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('RegisterCtrl', RegisterCtrl);

  RegisterCtrl.$inject = ['$state','AdminService', 'toastr','ngDialog'];
  function RegisterCtrl($state, AdminService, toastr,ngDialog) {
    var vm = this;

    vm.register = register;
    var openedDialog =null;


    function register() {
      vm.dataLoading = true;
      vm.message = "Account Created !! We need to verify that this email address belongs to you. Check your email and follow the instructions in the message we sent you.";


      AdminService.Create(vm.user).then(function (res) {
        vm.dataLoading = false;
        if (res.success) {
          toastr.info(vm.message);
          $state.go('page.login');

        }
      });

    }
  }

})();
