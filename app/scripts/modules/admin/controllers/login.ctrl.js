/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold login function
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['$state', 'AuthenticationService', 'toastr'];
  function LoginCtrl($state, AuthenticationService, toastr) {
    var vm = this;

    vm.login = login;

    function login() {
      vm.dataLoading = true;

      AuthenticationService.Login({email:vm.username, password:vm.password}).then(function (response) {
        vm.dataLoading = false;
        if (response.success) {
            toastr.success('You have successfully signed in');
            AuthenticationService.SetCredentials(response.data);
            console.log(response.data);
            $state.go('app.dashboard');
          }
      });
    }

  }

})();
