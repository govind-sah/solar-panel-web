/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc this will hold function to send instructions to reset password
 */

(function () {
  'use strict';

  angular
    .module('com.module.admin')
    .controller('ForgotPasswordCtrl', ForgotPasswordCtrl);

  ForgotPasswordCtrl.$inject = ['AdminService', 'toastr', '$route'];
  function ForgotPasswordCtrl(AdminService, toastr, $route) {
    var vm = this;

    vm.sendInstruction = sendInstruction;

    function sendInstruction() {
      vm.dataLoading = true;

      AdminService.SendInsToResetPassword(vm.email)
        .then(function (response) {
          if (response.success) {
            toastr.success('Instruction sent successful');
            vm.dataLoading = false;
            $route.reload();
          }
          vm.dataLoading = false;
        });
    }
  }

})();
