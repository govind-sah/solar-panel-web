/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc plants module
 */

(function () {
  'use strict';
  angular.module('com.module.plants',['highcharts-ng'])
    .config(config);

  config.$inject = [];
  function config() {
    /**
     * Grid-light theme for Highcharts JS
     * @author Torstein Honsi
     */

    Highcharts.theme = {
      colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
        "#55BF3B", "#DF5353", "#7798BF", "#aaeeee","#2C159D","#ED3D45"],
      chart: {
        backgroundColor: null
      },
      title: {
        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          textTransform: 'uppercase'
        }
      },
      tooltip: {
        borderWidth: 2,
        backgroundColor: 'rgba(255,255,255,0.8)',
        shadow: true,
        style: {
          fontSize: "12px"
        }
      },
      legend: {
        itemStyle: {
          fontWeight: 'bold',
          fontSize: '12px'
        }
      },
      xAxis: {
        gridLineWidth: 1,
        labels: {
          style: {
            fontSize: '10px'
          }
        }
      },
      yAxis: {
        minorTickInterval: 'auto',
        title: {
          style: {
            textTransform: 'uppercase'
          }
        },
        labels: {
          style: {
            fontSize: '12px'
          }
        }
      },
      plotOptions: {
        candlestick: {
          lineColor: '#404048'
        }
      },

      // General
      background2: '#F0F0EA'

    };

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
  }

})();
