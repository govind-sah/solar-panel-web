/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc Edit plant device
 */

(function () {
  'use strict';

  angular
    .module('com.module.plants')
    .controller('EditDeviceCtrl', EditDeviceCtrl);

  EditDeviceCtrl.$inject = ['$state', 'ApiService', '$stateParams', 'toastr'];
  function EditDeviceCtrl($state, ApiService, $stateParams, toastr) {
    var vm = this;

    vm.currentPageState = $state.current.stateDesc;
    // Function declarations goes here
    vm.updateDevice = updateDevice;
    vm.removeMeter = removeMeter;
    vm.removeInverter = removeInverter;
    vm.energyCalculationCriteria =[
      {
        name:'Inverter',
        value:'inverter'
      }, {
        name:'Meter',
        value:'meter'
      }, {
        name:'None',
        value:'none'
      }
    ];

    (function initController(){
      getDeviceDetails();
    }());

    function getDeviceDetails() {
      var requestPayload = {
        projection: {"_id": 0, "powerPlantId": 0, "createdAt": 0,"updatedAt": 0,"isDeleted": 0, "__v": 0},
        criteria: {},
        options: {}
      };

      var path  = '/powerPlants/' + $stateParams.id + '/deviceMacs/' + $stateParams.deviceId;
      ApiService.GetById(requestPayload, path).then(function (res) {
        if (res.success) {
          vm.payload = res.data;
        }
      });
    }

    function updateDevice() {
      vm.dataLoading = true;

      var path  = '/powerPlants/' + $stateParams.id + '/deviceMacs/' + $stateParams.deviceId;

      ApiService.Update(vm.payload, path)
        .then(function (response) {
          vm.dataLoading = false;
          if (response.success) {
            toastr.success('Updated successfully!');
          }
        });
    }

    function removeMeter(index) {
      vm.payload.meters.splice(index, 1);
    }

    function removeInverter(index) {
      vm.payload.inverters.splice(index, 1);
    }

  }

})();
