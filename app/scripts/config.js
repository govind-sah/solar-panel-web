/**
 * @author Govind Sah er.govindsah@gmail.com
 * @desc main configuration module
 */

(function() {
  "use strict";

  var local = {
    name: 'development',
    apiUrl: 'http://192.168.1.6:80/api',
    siteUrl:'http://localhost:9000'
  };

  var serverDev = {
    name: 'development',
    apiUrl: 'http://dashboard.fourthpartner.co/api',
    siteUrl:'http://52.24.213.154'
  };

  var serverLive = {
    name: 'Live',
    apiUrl: 'http://dashboard.fourthpartner.co/api',
    siteUrl:'http://52.24.213.154'
  };

  angular.module('config', [])

    .constant('ENV', serverDev)
    .constant('PROJECT_DETAILS',{NAME : "Solar Power"} );

})();
